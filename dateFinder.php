<?

	namespace dronz\wikiapi;

	class dateFinder{

		public  $parsedDate     = array();
		private $dateString, $days, $months, $years;
		private $monthNames     = array();
		private $longMonthNames = array('january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december');

		public function __construct($dateString = FALSE){
			foreach($this->longMonthNames as $month){
				$this->monthNames[] = substr($month, 0, 3);
			}
			if(isset($dateString) && $dateString){
				$dateString       = preg_replace("#\([^\)]+\)#u", '', $dateString);
				$dateString       = preg_replace("#\[[^\]]+\]#u", '', $dateString);
				$dateString       = preg_replace("#{{[^{}]+}}#u", '', $dateString);
				$dateString       = preg_replace("#\d+\s(am|pm)#ui", '', $dateString);
				$dateString       = preg_replace("#\d+:\d+#ui", '', $dateString);
				$dateString       = strtr($dateString, [
					'Start date and age' => '',
					'ref '               => ' ',
					'<br/>'              => "\n",
					'df=yes'             => " ",
					'start date and age' => "",
					'start date'         => "",
					'end date'         => "",
				]);
				$this->dateString = filter_var($dateString, FILTER_SANITIZE_STRING);
				$this->findDate();
			}
			else{
				return FALSE;
			}
		}

		public function findDate(){
			$this->parsedDate = array();

			if($this->simpleDate() === TRUE) return $this->parsedDate;

			$this->maybeYears();
			$this->maybeMonths();

			$this->maybeDays();
			$this->maybeInt();
			$this->maybeCentury();

			if(($this->years === TRUE) && ($this->months === FALSE) && ($this->days === FALSE) && ($this->checkUSFQD() === FALSE) && ($this->checkUKFQD() === FALSE)){
				//$this->parsedDate['day']   = $this->parsedDate['month'] = 1;
				//$this->parsedDate['range'] = TRUE;

				return $this->parsedDate;
			}

			if(($this->years === TRUE) && ($this->months === TRUE) && ($this->days === FALSE) && ($this->checkUSFQD() === FALSE) && ($this->checkUKFQD() === FALSE)){
				//$this->parsedDate['day']   = 1;
				//$this->parsedDate['range'] = TRUE;

				return $this->parsedDate;
			}

			if(($this->years === FALSE) && ($this->months === TRUE) && ($this->days === FALSE) && ($this->checkUSFQD() === FALSE) && ($this->checkUKFQD() === FALSE)){
				//$this->parsedDate['year'] = (int)date('Y');
				//$this->parsedDate['day']   = 1;
				//$this->parsedDate['range'] = TRUE;

				return FALSE;
			}

			if(($this->years === FALSE) && ($this->months === FALSE) && ($this->days === TRUE) && ($this->checkUSFQD() === FALSE) && ($this->checkUKFQD() === FALSE) && (strtotime($this->dateString) === FALSE) && isset($this->parsedDate['day'])){
				//$this->parsedDate['year']  = (int)date('Y');
				//$this->parsedDate['month'] = (int)date('n');
				return $this->parsedDate;
			}

			if($this->years === TRUE && $this->months === TRUE && $this->days === TRUE && ($this->checkUSFQD() === FALSE) && ($this->checkUKFQD() === FALSE)){
				return $this->parsedDate;
			}

			if($this->checkUSFQD() === TRUE) return $this->parsedDate;
			if($this->checkUKFQD() === TRUE) return $this->parsedDate;
			if($this->fuzzyLogic() === TRUE) return $this->parsedDate;
			return FALSE;
		}

		private function simpleDate(){
			$this->parsedDate = array();
			preg_match('/([0-9]{1,2})(st|nd|rd|th)?([^A-Za-z0-9]+)('.implode('|', $this->longMonthNames).')([^A-Za-z0-9]+)([0-9]{4})/i', $this->dateString, $matches);
			if(isset($matches[0])){
				$month = array_search(strtolower($matches[4]), $this->longMonthNames) + 1;
				if(checkdate($month, $matches[1], $matches[6])){
					$this->parsedDate['year']  = (int)$matches[6];
					$this->parsedDate['month'] = (int)$month;
					$this->parsedDate['day']   = (int)$matches[1];

					return TRUE;
				}
			}
		}

		private function maybeYears(){
			$year = FALSE;
			preg_match_all('/\b(\d{3,4})\b/', $this->dateString, $fqYears);
			$testString = html_entity_decode($this->dateString, ENT_QUOTES);
			preg_match_all("/\s['’](\d{2})\b/", $testString, $abbrYears);
			if(count($fqYears[1])){
				foreach($fqYears[1] as $year){
					break;
				}
			}
			if(count($abbrYears[1])){
				foreach($abbrYears[1] as $year){
					$year = (((int)$year >= 0) && ((int)$year <= 9)) ? '190'.$year : '19'.$year;
					break;
				}
			}
			if(\preg_match('#\b(SC)?BC\W?#isu', $this->dateString)){
				$year *= -1;
			}


			if($year){
				$this->parsedDate['year'] = (int)$year;
				$this->years              = TRUE;
			}
			else{
				$this->years = FALSE;
			}
		}

		private function maybeMonths(){
			$this->months = FALSE;
			if(preg_match_all('/\b('.implode('|', $this->longMonthNames).')\b/i', $this->dateString, $fqMonths)){
				$monthNum = array_search(strtolower($fqMonths[1][0]), $this->longMonthNames);
				if(is_int($monthNum) && ($monthNum >= 0) && ($monthNum <= 11)){
					$this->parsedDate['month'] = $monthNum + 1;
					$this->months              = TRUE;
				}
			}
			if(preg_match_all('/\b('.implode('|', $this->monthNames).')\b/i', $this->dateString, $abbrMonths)){
				$monthNum = array_search(strtolower($abbrMonths[1][0]), $this->monthNames);
				if(is_int($monthNum) && ($monthNum >= 0) && ($monthNum <= 11)){
					$this->parsedDate['month'] = $monthNum + 1;
					$this->months              = TRUE;
				}
			}
		}

		private function maybeDays(){
			preg_match_all('/\b([0-9]{1,2})(st|nd|rd|th)?\b/', $this->dateString, $days);
			$this->days = FALSE;
			if(count($days[1])){
				foreach($days[1] as $test){
					if(((int)$test >= 1) && ((int)$test <= 31)){
						$this->parsedDate['day'] = (int)$test;
						$this->days              = TRUE;
						break;
					}
				}
			}
		}

		private function maybeInt($df = true){
			$pattern = $df ? '#(\d{4})(\d{1,2})(\d{2})#' : '#(\d{4})(\d{2})(\d{1,2})#';
			preg_match_all($pattern, $this->dateString, $result);
			if(count($result[1])){
				foreach($result[1] as $k => $year){
					$day   = (int)$result[3][$k];
					$month = (int)$result[2][$k];
					if($day > 31 || $month > 12){
						continue;
					}
					$this->parsedDate['day']   = (int)$day;
					$this->parsedDate['month'] = (int)$month;
					$this->parsedDate['year']  = (int)$year;
					$this->days                = TRUE;
					$this->years               = TRUE;
					$this->months              = TRUE;
					break;
				}
			}

			if(!$this->years && $df){
				return $this->maybeInt(!$df);
			}

		}

		private function maybeCentury(){
			preg_match_all('#\b(\d{1,2})(-\d+)?(st|nd|rd|th).*?(century|centuries)#', $this->dateString, $year);
			if(count($year[1])){
				foreach($year[1] as $year){
					$this->parsedDate = ['year' => (int)$year * 100];
					$this->years      = TRUE;
					$this->days       = FALSE;
					$this->months     = FALSE;
					break;
				}
			}
		}

		private function checkUSFQD(){
			$USFQD  = explode('-', $this->dateString);
			$errors = 0;
			if(count($USFQD) === 3 && preg_match('#\d+-\d+-\d+#', $this->dateString)){
				$this->parsedDate = array();
				if(((int)$USFQD[0] < 1) && ((int)$USFQD[0] > 12)) $errors++;
				if(((int)$USFQD[1] < 1) && ((int)$USFQD[1] > 31)) $errors++;
				if(!is_numeric($USFQD[2])) $errors++;
				if($errors === 0){
					if(checkdate((int)$USFQD[0], (int)$USFQD[1], (int)$USFQD[2])){
						$this->parsedDate['year']  = (int)$USFQD[2];
						$this->parsedDate['month'] = (int)$USFQD[0];
						$this->parsedDate['day']   = (int)$USFQD[1];

						return TRUE;
					}
					else{
						return FALSE;
					}
				}
				else{
					return FALSE;
				}
			}
			else{
				return FALSE;
			}
		}

		private function checkUKFQD(){
			$UKFQD  = explode('/', $this->dateString);
			$errors = 0;
			if(count($UKFQD) === 3){
				$this->parsedDate = array();
				if(((int)$UKFQD[0] < 1) && ((int)$UKFQD[0] > 31)) $errors++;
				if(((int)$UKFQD[1] < 1) && ((int)$UKFQD[1] > 12)) $errors++;
				if(!is_numeric($UKFQD[2])) $errors++;
				if($errors === 0){
					if(checkdate((int)$UKFQD[1], (int)$UKFQD[0], (int)$UKFQD[2])){
						$this->parsedDate['year']  = (int)$UKFQD[2];
						$this->parsedDate['month'] = (int)$UKFQD[1];
						$this->parsedDate['day']   = (int)$UKFQD[0];

						return TRUE;
					}
					else{
						return FALSE;
					}
				}
				else{
					return FALSE;
				}
			}
			else{
				return FALSE;
			}
		}

		private function fuzzyLogic(){
			$fuzzy = date_parse($this->dateString);
			if(($fuzzy['error_count'] === 0) && ($fuzzy['warning_count'] === 0)){
				foreach(['year','month','day'] as $part){
					if(isset($fuzzy[$part]) && $fuzzy[$part]){
						$this->parsedDate[$part]=$fuzzy[$part];
					}
				}
				//if(($this->days === FALSE) && ($this->parsedDate['day'] === 1)) $this->parsedDate['range'] = TRUE;

				return TRUE;
			}
			else{
				$timestamp = getdate(strtotime($this->dateString));
				if($timestamp[0] && false){
					if(checkdate($timestamp['mon'], $timestamp['mday'], $timestamp['year'])){
						$this->parsedDate['year']  = (int)$timestamp['year'];
						$this->parsedDate['month'] = (int)$timestamp['mon'];
						$this->parsedDate['day']   = (int)$timestamp['mday'];
						if(
							(($this->days === FALSE) && ($this->parsedDate['day'] === 1)) ||
							(($this->months === TRUE) && empty($this->days) && ($this->years === FALSE))
						){
							//$this->parsedDate['day'] = 1;
						}
						return TRUE;
					}
					else{
						return FALSE;
					}
				}
				else{
					$year = $month = $day = FALSE;
					preg_match_all('/('.implode('|', $this->monthNames).')/i', $this->dateString, $monthMatch);
					if(count($monthMatch)){
						foreach($monthMatch as $foundMonth){
							if(isset($foundMonth[0]) && $foundMonth[0]) $month = array_search(strtolower($foundMonth[0]), $this->monthNames) + 1;
							preg_match('/([0-9]?[0-9])(st|nd|rd|th)/', $this->dateString, $dayMatch);
							if(count($dayMatch)) if($dayMatch[1]) $day = $dayMatch[1];
							preg_match('/[0-9]{4}/', $this->dateString, $yearMatch);
							if(count($yearMatch) && $yearMatch[0]) $year = $yearMatch[0];
							if($year && $month && $day){
								if(checkdate($month, $day, $year)){
									$this->parsedDate['year']  = (int)$year;
									$this->parsedDate['month'] = (int)$month;
									$this->parsedDate['day']   = (int)$day;

									return TRUE;
								}
								else{
									return FALSE;
								}
							}
							else{
								return FALSE;
							}
						}
					}

					return FALSE;
				}
			}

			return FALSE;
		}
	}