<?php
	/**
	 * Created by PhpStorm.
	 * User: dronz
	 * Date: 17.7.18
	 * Time: 11.35
	 */

	namespace dronz\wikiapi;


	use yii\validators\Validator;

	class WikiTitleValidator extends Validator{

		protected function validateValue($value){
			$api  = new WikiScraper();
			$page = $api->findByTitle($value);

			return $page ? NULL : ['Wiki page {value} not found', []];
		}
	}