<?php

	namespace dronz\wikiapi;

	use yii\base\BaseObject;
	use yii\helpers\ArrayHelper;

	/**
	 * Created by PhpStorm.
	 * User: dronz
	 * Date: 3.7.18
	 * Time: 16.52
	 */

	class WikiScraper extends BaseObject{

		public $endpoint='https://{lang}.wikipedia.org/w/api.php';
		public $language='en';

		public function init(){
			$this->endpoint=strtr($this->endpoint, ['{lang}'=>$this->language]);
		}

		public function getById($id){
			$params['pageids']=$id;
			$result = $this->query($params);
		}

		public function findByTitle($title, $params=[]){
			$params['titles']=$title;
			$result=$this->query($params);
			$pid=key($result->query->pages);
			$page=$result->query->pages;
			if($pid==-1){
				return [];
			}
			return $page;
		}

		public function findByQuery($query,$params=[]){
			//https://en.wikipedia.org/w/api.php?action=query&format=jsonfm&prop=coordinates&list=search&srsearch=the+moscow+kremlin+russia
			$params['list']='search';
			$params['srsearch']=$query;
			$result=$this->query($params);
			$search=$result->query->search;
			return empty($search) ? [] : $search;
		}

		public function opensearch($query,$params=[]){
			//https://en.wikipedia.org/w/api.php?action=opensearch&search=Dar-ul-Aman%20Palace&limit=1&namespace=0&format=jsonfm
			$params['action']='opensearch';
			$params['search']=$query;
			$params['limit']=1;
			$params['namespace']=0;
			$result=$this->query($params);
			if(!empty($result[1])){
				return $result[1][0];
			}

			return false;

		}

		public function geo($lat,$lon,$radius=1000,$params=[]){
			//https://en.wikipedia.org/w/api.php?action=query&format=jsonfm&list=geosearch&gscoord=55.75202330|37.61749940&gsradius=100
			$params['list']='geosearch';
			$params['gscoord']="$lat|$lon";
			$params['gsradius']=$radius;
			$result=$this->query($params);
			$search=$result->query->geosearch;
			return empty($search) ? [] : $search;

		}

		public function query($params){
			$params['action']=ArrayHelper::getValue($params, 'action','query');
			$params['format']='json';
			$url=$this->endpoint.'?'.http_build_query($params);
			if(\Yii::$app->request->isConsoleRequest){
				echo $url.\PHP_EOL;
			}
			$data=json_decode(file_get_contents($url));



			return $data;
		}

		/**@return \DOMXPath */
		public function getPage($pageId, $params = []){
			//https://en.wikipedia.org/w/api.php?action=parse&pageid=737&prop=parsetree&contentmodel=json&sectiontitle=Formation
			$params['action'] = 'parse';
			$params['pageid'] = $pageId;
			$params['prop']   = 'parsetree';

			$result = $this->query($params);
			$xml    = $result->parse->parsetree->{'*'};
			if(strpos($xml, '<root>#REDIRECT') !== FALSE && !isset($params['redirects'])){
				$params['redirects'] = 1;

				return $this->getPage($pageId, $params);
			}
			if($xml){
				$m = new \DOMDocument();
				$m->loadXML($xml);
				$data = new \DOMXPath($m);

				return $data;
			}

		}

	}