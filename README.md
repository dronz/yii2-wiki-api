wiki api
========
wiki api

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist dronz/yii2-wiki-api "*"
```

or add

```
"dronz/yii2-wiki-api": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \\dronz\wikiapi\AutoloadExample::widget(); ?>```