<?php


	use dronz\wikiapi\dateFinder;

	class WikiParserHelperCest{

		public function _before(UnitTester $I){
		}

		public function _after(UnitTester $I){
		}


		/**
		 * @dataProvider datesProvider
		 */
		public function parseDate(UnitTester $I, \Codeception\Example $data){
			list($actual, $expected) = $data;
			$actual = (new dateFinder($actual))->findDate();
			$I->assertEquals($expected, $actual);

		}

		private function datesProvider(){
			/*return [
				['1960s',['year'=>1960]]
			];*/
			return [
				['1960s', ['year' => 1960]],
				[
					'10th-16th centuriesref{{Cite book|url=https://www.worldcat.org/oclc/832701265|title=Kazanʹ : portret v stile impressionizma|last=Sergeĭ,|first=Sokolov,|isbn=9785922204088|edition=2-e izdanie, dopolnitelʹnoe|location=Kazanʹ|oclc=832701265}}',
					['year' => 1000]
				],
				['2000 (24th [[World Heritage Committee|session]])', ['year' => 2000]],
				['start date1932119df=y', ['year' => 1932, 'month' => 1, 'day' => 19]],
				//['Start date1982111df=y', ['year' => 1982, 'month' => 11, 'day' => 1]],
				['unbulleted list \'\'\'Post Office\'\'\': end date1879 \'\'\'Government House\'\'\': end date1886 \'\'\'Unification\'\'\': end date1898
', ['year' => 1879]],
				['2nd century AD,<!-- {{Start date|YYYY}} -->,90s of the 1st century AD,12th century,4th century', ['year' => 200]],
				['start date1528', ['year' => 1528]],

				['2009', ['year' => 2009]],
				['7 AM', FALSE],
				['9999', ['year' => 9999]],
				['9999 <small>(99th [[World Heritage Committee|session]])</small>', ['year' => 9999]],
				['23 June 1984', ['year' => 1984, 'day' => 23, 'month' => 6]],
				['Start date99991010', ['year' => 9999, 'day' => 10, 'month' => 10]],
				['February 15, 9999', ['year' => 9999, 'day' => 15, 'month' => 2]],
				['1 August 9999', ['year' => 9999, 'day' => 1, 'month' => 8]],
				['dd mm yyyy ([[Anno Domini|AD]])', FALSE],
				['15 February 999', ['year' => 999, 'day' => 15, 'month' => 2]],
				['1 September 99', ['year' => 1999, 'day' => 1, 'month' => 9]],
				['9999 (9rd [[World Heritage Committee|session]])', ['year' => 9999]],
				['99th century', ['year' => 9900]],
				['9 June 9999', ['year' => 9999, 'day' => 9, 'month' => 6]],
				['9 January 9999 to 20 May 9999', ['year' => 9999, 'day' => 9, 'month' => 1]],
				['August 9st 9999<br>small(celebrated September 20th)', ['year' => 9999, 'day' => 9, 'month' => 8]],
				['start date 9999', ['year' => 9999]],
				['9th–99th century', ['year' => 900]],
				['start date and age9999', ['year' => 9999]],
				['start date and age99991010p=yes', ['year' => 9999, 'day' => 10, 'month' => 10]],
				['Start date and years agodf=yes99991010', ['year' => 9999, 'day' => 10, 'month' => 10]],
				['9999 (as a Museum)', ['year' => 9999]],
				[
					'20 August 9999ref name="Raymond9999"{{cite book|last=Raymond|first=Gino|title=Historical dictionary of France|url=https://books.google.com / books ? id = JVIRzOWyqUAC & pg = PA9 | accessdate = 99 July 9999 | date = 99 October 9999 | publisher = Scarecrow Press | isbn = 999 - 9 - 9999 - 9999 - 9 | page = 9}}ref > ',
					['year' => 9999, 'day' => 20, 'month' => 8]
				],
				[
					'10 July 9999ref name = "Fleischmann9999"{ { cite book | last = Fleischmann | first = Hector | title = An unknown son of Napoleon | url = https://books.google.com/books?id=ZnVAAAAAYAAJ&pg=PA999|accessdate=99 July 9999|year=9999|publisher=John Lane company|page=999}}</ref>',
					['year' => 9999, 'day' => 10, 'month' => 7]
				],
				['end datedf=yes9999', ['year' => 9999]],
				['May 10, 9999', ['year' => 9999, 'day' => 10, 'month' => 5]],
				['December 10, 9999ref name="leszoosdanslemonde"', ['year' => 9999, 'day' => 10, 'month' => 12]],
				['1961-64, 9999', ['year' => 1961]],
				[
					'start date and age9999ref{{cite web|url=http://www.zoo-frankfurt.de/unser-zoo/geschichte/|title=Kurzer Überblick der Geschichte des Frankfurter Zoos | language = German | accessdate = 9 May 9999} }</ref > ',
					['year' => 9999, 'day' => 9, 'month' => 5]
				],
				['9 August 9999ref name = "zoo_history"', ['year' => 9999, 'day' => 9, 'month' => 8]],
				['c. 9999 (opened)', ['year' => 9999]],
				['Rebuilt 9999', ['year' => 9999]],
				['April 9999', ['year' => 9999, 'month' => 4]],
				['9999-9999', ['year' => 9999]],
				['9999 (Temple), 9999 (with Adi Granth) sfnArvind-Pal Singh Mandair9999pp=99-99', ['year' => 9999]],
				['December 9999sfnArvind-Pal Singh Mandair9999pp=41-42', false],
				['9999ref name="tips"', ['year' => 9999]],
				['c. 9999', ['year' => 9999]],
				['January 10,9999', ['year' => 9999, 'month' => 1, 'day' => 10]],
				['<!-- {{Start date|df=y|YYYY|MM|DD}} -->', FALSE],
				['9999 <small>(9nd&nbsp;[[World Heritage Committee|session]])</small>', ['year' => 9999]],
				['9999–9999<br/>9999–9999', ['year' => 9999]],
				['9999 <small>(99rd [[World Heritage Committee|session]])</small>', ['year' => 9999]],
				['9999ref name="rasnov9"', ['year' => 9999]],
				[
					'10 April 9999 ([[soft opening]])<br />10 June 9999 ([[official]] opening)<br />10 February 9999 ([[grand opening]])',
					['year' => 9999, 'day' => 10, 'month' => 4]
				],
				['10 May 9999', ['year' => 9999, 'day' => 10, 'month' => 5]],
				['July 10, 9999', ['year' => 9999, 'day' => 10, 'month' => 7]],
				['start date and agedf=yes99990503', ['year' => 9999, 'day' => 3, 'month' => 5]],
				['9999, 9999', ['year' => 9999]],
				['9 November 9999', ['year' => 9999, 'day' => 9, 'month' => 11]],
				['start date1959331ref name="jt"', ['year' => 1959, 'day' => 31, 'month' => 3]],
				['circa 9999–9999 BC ([[Fourth dynasty of Egypt|9th dynasty]])', ['year' => -9999]],
				['Start date99990305df=y', ['year' => 9999, 'day' => 5, 'month' => 3]],
				['Start date and ageOctober 10, 9999', ['year' => 9999, 'day' => 10, 'month' => 10]],
				['April 10, 9999', ['year' => 9999, 'day' => 10, 'month' => 4]],
				['March 9999', ['year' => 9999, 'month' => 3]],
				['c.9999', ['year' => 9999]],
				['99th – 99th century', ['year' => 9900]],
				['birth date and age1974217df=y', ['year' => 1974, 'day' => 17, 'month' => 2]],
				['999 BCE', ['year' => -999]],
				['999–9999', ['year' => 999]],
				['9999–9999', ['year' => 9999]],
				['9 March 9999refngroup=notename=one99 February in the [[Julian calendar]] used at the time.', ['year' => 9999, 'day' => 9, 'month' => 3]],
				['9 October 9999refngroup=notename=two99 September in the [[Julian calendar]] used at the time.', ['year' => 9999, 'day' => 9, 'month' => 10]],
				['&nbsp;', FALSE],
				['January 9, 9999', ['year' => 9999, 'day' => 9, 'month' => 1]],
				['January 9999', ['year' => 9999, 'month' => 1]],
				['dd/mm/yyyy ([[Anno Domini|AD]])', FALSE],
				['[[Circa|c.]] 9999 scbc', ['year' => -9999]],
				['<!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) -->', FALSE],
				['birth datedf=yes1867425', ['year' => 1867, 'day' => 25, 'month' => 4]],
				['October 9999 (aged 10)', ['year' => 9999, 'month' => 10]],
			];
		}
	}
