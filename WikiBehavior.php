<?php
	/**
	 * Created by PhpStorm.
	 * User: dronz
	 * Date: 5.6.18
	 * Time: 10.42
	 */

	namespace dronz\wikiapi;


	use yii\base\Behavior;
	use yii\helpers\Url;
	use yii\validators\Validator;

	class WikiBehavior extends Behavior{

		public $unique = TRUE;

		public function wikiUrl($lang = 'en'){
			return $this->owner->wiki_title ? Url::to(["http://$lang.wikipedia.org/wiki/{$this->owner->wiki_title}"], 'https') : NULL;
		}

		/**
		 * @param \yii\db\ActiveRecord $owner
		 */
		public function attach($owner){
			parent::attach($owner);

			if($this->unique){
				$unique = Validator::createValidator('unique', $owner, ['wiki_title']);
				$owner->validators->append($unique);
			}

			$self = Validator::createValidator(WikiTitleValidator::class, $owner, 'wiki_title');
			$owner->validators->append($self);
		}
	}