<?php
	/**
	 * Created by PhpStorm.
	 * User: dronz
	 * Date: 11.7.18
	 * Time: 15.53
	 */

	namespace dronz\wikiapi;


	class WikiParserHelper{

		public static function parseInfobox(\DOMXPath $dom){
			$nodes  = $dom->query('template[starts-with(title,"Infobox")]/part');
			$result = [];
			foreach($nodes as $node){
				/**@var \DOMElement $node */

				list($name, $value) = self::parsePart($node);
				$result[$name] = $value;
			}

			return $result;
		}

		public static function parsePart(\DOMElement $el){
			$name = $value = NULL;
			foreach($el->childNodes as $node){
				/**@var \DOMElement $node */
				if($node->nodeName == 'name'){
					$name = trim($node->nodeValue);
				}
				if($node->nodeName == 'value'){
					$value = trim($node->nodeValue);
				}
			}

			return [$name, $value];
		}

	}